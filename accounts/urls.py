from django.urls import path
from django.contrib.auth import views as auth_views
from accounts.views import user_signup


urlpatterns = [
    path("login/", auth_views.LoginView.as_view(), name="login"),
    path("logout/", auth_views.LogoutView.as_view(), name="logout"),
    path("signup/", user_signup, name="signup"),
]
