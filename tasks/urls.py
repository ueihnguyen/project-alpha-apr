from django.urls import path
from tasks.views import TaskCreateView, TaskListView, update_task_status

urlpatterns = [
    path("create/", TaskCreateView.as_view(), name="create_task"),
    path("mine/", TaskListView.as_view(), name="show_my_tasks"),
    path("<int:pk>/complete/", update_task_status, name="complete_task"),
]
