from django.shortcuts import redirect

# Create your views here.

from tasks.models import Task
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.decorators.http import require_http_methods


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "projects/task_new.html"
    fields = [
        "name",
        "start_date",
        "due_date",
        "project",
        "assignee",
    ]

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.project.id])


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "projects/task_list.html"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


@require_http_methods(["POST"])
def update_task_status(request, pk):
    status = request.POST.get("is_completed")
    task = Task.objects.get(pk=pk)
    task.is_completed = status
    task.save()
    return redirect("show_my_tasks")
